package persona;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import persona.excepcion.PersonaIncompletaException;
import persona.modelo.Persona;

import java.time.LocalDate;

public class PersonaModelUnitTest {

    @Test
    void instancia_TodosLosAtributosCorrectos_InstanciaPersona() {
        //Arrange
        //Act
        Persona laPersona = Persona.instancia(1, "Nombre", "12345678", LocalDate.of(2000, 1, 1));
        //Assert
        Assertions.assertNotNull(laPersona);
        Assertions.assertEquals("Nombre", laPersona.getNombre());
    }

    @Test
    void instancia_FaltaNombre_PersonaIncompletaException() {
        //Arrange
        //Act
        Exception exceptionVacio = Assertions.assertThrows(PersonaIncompletaException.class, () -> Persona.instancia(1, "", "12345678", LocalDate.of(2000, 1, 1)));
        Exception exceptionNulo = Assertions.assertThrows(PersonaIncompletaException.class, () -> Persona.instancia(1, null, "12345678", LocalDate.of(2000, 1, 1)));
        //Assert
        Assertions.assertEquals("El nombre de la persona es obligatorio", exceptionVacio.getMessage());
        Assertions.assertEquals("El nombre de la persona es obligatorio", exceptionNulo.getMessage());
    }

}
