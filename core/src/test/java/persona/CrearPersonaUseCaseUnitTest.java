package persona;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import persona.excepcion.PersonaYaExisteException;
import persona.input.CrearPersonaInput;
import persona.interactor.CrearPersonaInteractor;
import persona.modelo.Persona;
import persona.output.CrearPersonaRepositorio;

import java.time.LocalDate;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CrearPersonaUseCaseUnitTest {

        /*TODO:
       - Validar en la base si existe persona por dni
       - SI existe -> Aviso que existe y no creo (Exception)
       - SI NO Existe -> Crea la persona
       - Devolver nuevo ID
     */

    CrearPersonaInput crearPersonaInteractor;

    @Mock
    CrearPersonaRepositorio crearPersonaRepositorio;

    @BeforeEach
    void setup(){
        crearPersonaInteractor=new CrearPersonaInteractor(crearPersonaRepositorio);
    }


    @Test
    void creaPersona_PersonaNoExiste_CreaPersona(){
        Persona laPersona=Persona.instancia(null,"Perez", "123", LocalDate.of(2000,1,1));

        when(crearPersonaRepositorio.existePorDNI("123")).thenReturn(false);
        when(crearPersonaRepositorio.guardarPersona(laPersona)).thenReturn(1);

        Assertions.assertEquals(1,crearPersonaInteractor.crearPersona(laPersona));
    }

    @Test
    void creaPersona_PersonaYaExiste_PersonaYaExisteException(){
        Persona laPersona=Persona.instancia(null,"Perez", "123", LocalDate.of(2000,1,1));

        when(crearPersonaRepositorio.existePorDNI("123")).thenReturn(true);
        verify (crearPersonaRepositorio,never()).guardarPersona(laPersona);

        Exception exception = Assertions.assertThrows(PersonaYaExisteException.class,()->crearPersonaInteractor.crearPersona(laPersona));
        Assertions.assertEquals("Ya existe persona con dni 123", exception.getMessage());
    }
}

