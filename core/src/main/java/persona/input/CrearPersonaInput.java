package persona.input;

import persona.modelo.Persona;

public interface CrearPersonaInput {
    Integer crearPersona(Persona laPersona);
}
