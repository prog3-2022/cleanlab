package persona.modelo;

import persona.excepcion.PersonaIncompletaException;

import java.time.LocalDate;

public class Persona{
    private Integer id;
    private String nombre;
    private String documento;
    private LocalDate fechaNacimiento;

    private Persona(Integer id, String nombre, String documento, LocalDate fechaNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.documento = documento;
        this.fechaNacimiento = fechaNacimiento;
    }

    public static Persona instancia(Integer id, String nombre, String documento, LocalDate fechaNacimiento) {
        if(nombre==null || nombre.isEmpty()){
            throw new PersonaIncompletaException("El nombre de la persona es obligatorio");
        }
        return new Persona(id, nombre, documento, fechaNacimiento);
    }

    public String getNombre() {
        return nombre;
    }

    public String getDocumento() {
        return documento;
    }
}
