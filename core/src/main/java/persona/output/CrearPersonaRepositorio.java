package persona.output;

import persona.modelo.Persona;

public interface CrearPersonaRepositorio {
    boolean existePorDNI(String dni);

    Integer guardarPersona(Persona laPersona);
}
