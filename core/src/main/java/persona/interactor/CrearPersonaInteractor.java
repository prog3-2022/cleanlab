package persona.interactor;

import persona.excepcion.PersonaYaExisteException;
import persona.input.CrearPersonaInput;
import persona.modelo.Persona;
import persona.output.CrearPersonaRepositorio;

public class CrearPersonaInteractor implements CrearPersonaInput {
    private CrearPersonaRepositorio crearPersonaRepositorio;

    public CrearPersonaInteractor(CrearPersonaRepositorio crearPersonaRepositorio) {
        this.crearPersonaRepositorio = crearPersonaRepositorio;
    }

    @Override
    public Integer crearPersona(Persona laPersona) {
        if (crearPersonaRepositorio.existePorDNI(laPersona.getDocumento())) {
            throw new PersonaYaExisteException(String.format("Ya existe persona con dni %s", laPersona.getDocumento()));
        }
        Integer idNuevo = crearPersonaRepositorio.guardarPersona(laPersona);
        return idNuevo;
    }
}
